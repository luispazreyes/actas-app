import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { url } from 'inspector';
import { URLSERVICE } from '../url/Url-Service';
import { element } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class ActasService {

  constructor( private http: HttpClient) { }

  actas: string[] = [];

  getActas( cv: string ) {
    let promise = new Promise( ( resolve, reject ) => {
      this.http.post(
         `${URLSERVICE}/actas/obtener_actas`,
         { cv }
      ).subscribe( (data: any) => {

        if(data){
          this.actas = [];
          data.forEach(element => {
            this.actas.push(element.MER);
          });
          resolve(true);
        } else{
          resolve(false);
        }
      });
    });

    return promise;
  }

  buscarActas( termino: string ){
    let actasArr: string[] = [];
    termino = termino.toLowerCase();

    for( let i = 0; i < this.actas.length; i ++ ){

      let acta = this.actas[i];

      let nombre: string = acta;

      nombre = nombre.toString();

      if( nombre.indexOf( termino ) >= 0  ){
        //acta.idx = i;
        actasArr.push( acta )
      }
    }

    return actasArr;
  }
}
