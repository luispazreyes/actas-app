import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ActasComponent } from './components/actas/actas.component';
import { ActasService } from './services/actas.service';

import { HttpClientModule } from '@angular/common/http';
import { ActaTarjetaComponent } from './components/acta-tarjeta/acta-tarjeta.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { BuscadorComponent } from './components/buscador/buscador.component';

@NgModule({
  declarations: [
    AppComponent,
    ActasComponent,
    ActaTarjetaComponent,
    NavbarComponent,
    BuscadorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ ActasService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
