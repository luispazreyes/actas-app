import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ActasService } from '../../services/actas.service';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent implements OnInit {

  actas: any[] = [];
  termino: string;

  constructor( private activatedRoute: ActivatedRoute, private _as: ActasService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe( params => {
      this.termino = params['termino'];
      this.actas = this._as.buscarActas( params['termino']);
    });
  }

}
