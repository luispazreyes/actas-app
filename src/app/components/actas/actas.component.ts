import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ActasService } from '../../services/actas.service';

declare var $: any;

@Component({
  selector: 'app-actas',
  templateUrl: './actas.component.html',
  styleUrls: ['./actas.component.css']
})
export class ActasComponent implements OnInit {

  centroVotacion: string;
  cargando: boolean = true;

  constructor( private activatedRoute: ActivatedRoute, public _as: ActasService) {
    this.activatedRoute.params.subscribe( (params: any) => {
      this.centroVotacion = params['cv'];
      this._as.getActas( params['cv']).then((res: boolean) => {
        if(res){
          this.cargando = false;
        }
      });
    });
  }

  ngOnInit() {

    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox();
    });
  }

}
