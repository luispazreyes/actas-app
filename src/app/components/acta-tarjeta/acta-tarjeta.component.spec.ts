import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActaTarjetaComponent } from './acta-tarjeta.component';

describe('ActaTarjetaComponent', () => {
  let component: ActaTarjetaComponent;
  let fixture: ComponentFixture<ActaTarjetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActaTarjetaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActaTarjetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
